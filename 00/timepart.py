import numpy as np
import matplotlib.pyplot as plt
import sys
import os
from os.path import isfile
import time

from matplotlib.colors import ListedColormap, LinearSegmentedColormap

print("hororing 3 seconds")
time.sleep(3)

from cauc import cauc

#tparts


N=100

cmap=np.zeros((2*N,4))

col1=np.array([255,0,68])
col2=np.array([0,21,255])

col1=col1/255
col2=col2/255

for i in range(N):
  alpha=1-i/N
  cmap[i]=[col1[0],col1[1],col1[2],alpha]
  cmap[2*N-i-1]=[col2[0],col2[1],col2[2],alpha]

cmap=ListedColormap(cmap)

def raster(p,c,y,idx):
  if isfile("raster.npz") and "l" in key:
    f=np.load("raster.npz")
    return f["aucs"],f["e30s"]
  d1=int(p.shape[-2])
  d2=int(p.shape[-1])

  aucs=np.zeros((d1,d2))
  e30s=np.zeros((d1,d2))


  for i1 in range(d1):
    for i2 in range(d2):
      ac=cauc(p=p[:,i1,i2],c=c[:,i1,i2],y=y)
      aucs[i1,i2]=ac["auc"]
      e30s[i1,i2]=ac["e30"]
      print("did",str(i1)+"/"+str(d1),str(i2)+"/"+str(d2))

  np.savez_compressed("traster/"+str(idx),aucs=aucs,e30s=e30s)

  return aucs,e30s

def partiate(p,c,y,d1l,d1h,d2l,d2h):
  return p[:,d1l:d1h,d2l:d2h],c[:,d1l:d1h,d2l:d2h],y
def partiate2(p,c,y,d):
  ap=np.reshape(p,(p.shape[0],p.shape[1]*p.shape[2]),order="F")
  ac=np.reshape(c,(c.shape[0],c.shape[1]*c.shape[2]),order="F")
  return ap[:,d],ac[:,d],y
def test(p,c,y):
  ac=cauc(p=p,c=c,y=y)
  return ac["auc"],ac["e30"],p.shape,ac


fil="evalb"

#fil="alleval/4"

#last one analysed
start=135

#last one to go
endd=1100



for something in range(10000):
  
  files=[f[:f.find(".")] for f in os.listdir("alleval/") if f.find(".")>0]

  np.random.shuffle(files)
  i0=0
  while i0<len(files):
    try:
      idd=int(files[i0])
      break
    except:
      i0+=1
  idd=files[0]

  fil="alleval/"+str(idd)

  key="raesp"

  try:
    x=np.load(fil+".npz")["x"]
  except:
    print("waiting a minute for ",fil)
    time.sleep(60)
  f=np.load(fil+".npz")

  x=f["x"]
  y=f["y"]
  p=f["p"]
  c=f["c"]


  if "p" in key:
    d=[]
    pn=int(c.shape[1])
    tn=int(c.shape[1])*int(c.shape[2])
    for i in range(tn-pn,tn):
      d.append(i)
    shallsaved=True

  if "pp" in key:
    d=[]
    pn=int(c.shape[1])
    tn=int(c.shape[1])*int(c.shape[2])
    for i in range(tn-2*pn,tn):
      d.append(i)
    shallsaved=True

  if "w" in key:
    d=[]
    pn=int(c.shape[1])
    tn=int(c.shape[1])*int(c.shape[2])
    for i in range(pn,3*pn):
      d.append(i)
    shallsaved=True

  if "w" in key and "p" in key:
    d=[]
    pn=int(c.shape[1])
    tn=int(c.shape[1])*int(c.shape[2])
    for i in range(tn-3*pn,tn):
      d.append(i)
    shallsaved=True





  if "r" in key:
    aucs,e30s=raster(p,c,y,idx=idd)
    print("raster")
    print("aucs")
    print(aucs)
    print("1/e30s")
    print(1/e30s)

    s1,s2=aucs.shape

    xx,yy=np.mgrid[0:s2:1,0:s1:1]

    xx=xx.flatten()
    yy=yy.flatten()


    if "a" in key:
      plt.pcolor(aucs,cmap=cmap,vmin=0.0,vmax=1.0)
      plt.title("auc")
      plt.ylabel("particle")
      plt.xlabel("feature")

      if "m" in key:
        for ad in d:
          plt.plot([xx[ad]+0.5],[yy[ad]+0.5],"o",color="black")



      if "s" in key:plt.savefig("tparts/aucmap"+str(idd)+".png",format="png")
      #if "s" in key:plt.savefig("imgs/aucmap"+str(idd)+".pdf",format="pdf")

      #plt.show()
      plt.close() 
    if "e" in key:
      plt.pcolor(1/e30s,cmap=plt.get_cmap("Reds"))
      plt.title("1/e30")
      plt.ylabel("particle")
      plt.xlabel("feature")

      if "m" in key:
        for ad in d:
          plt.plot([xx[ad]+0.5],[yy[ad]+0.5],"o",color="black")


      if "s" in key:plt.savefig("tparts/e30map"+str(idd)+".png",format="png")
      #if "s" in key:plt.savefig("imgs/e30map"+str(idd)+".pdf",format="pdf")

      #plt.show()
      plt.close() 


  d1l=1
  d1h=4
  d2l=0
  d2h=1
  #First Particle/feature, First Particle/second feature...


  #auc,e30,shape,data=test(*partiate(p,c,y,d1l,d1h,d2l,d2h))
  auc,e30,shape,data=test(*partiate2(p,c,y,d))
  auc_0,e30_0,shape_0,data_0=test(p,c,y)

  print("new",{"auc":auc,"1/e30":1/e30,"shape":shape})
  print("old",{"auc":auc_0,"1/e30":1/e30_0,"shape":shape_0})


  for j in range(100):print("did",idd)
  try:
    os.remove(fil+".npz")
  except:
    print("could not delete file...ignoring/assuming double")














import numpy as np

f=np.load("../599/evalb.npz")

cc=f["c"]
yy=f["y"]

cc=np.reshape(cc,[cc.shape[0],np.prod(cc.shape[1:])])

cc=cc[:,:4]

c=[]
y=[]

fractor=1#0
f2=100

for ac,ay in zip(cc,yy):
  if ay<0.5 or not np.random.randint(fractor):
    c.append(ac)
    y.append(ay)

c=np.array(c)
y=np.array(y)



# isolation forest for imbalanced classification
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score,auc,roc_auc_score
from sklearn.ensemble import IsolationForest
# generate dataset
X=c
# split into train/test sets
trainX, testX, trainy, testy = train_test_split(X, y, test_size=0.5, random_state=2, stratify=y)

trainX=np.array(list([ac for ac,ay in zip(trainX,trainy) if ay<0.5]))
trainy=np.array(list([ay for ay in trainy if ay<0.5]))

# define outlier detection model
model = IsolationForest(contamination=0.5/(fractor*f2), behaviour='new')
# fit on majority class
trainX = trainX[trainy==0]
model.fit(trainX)
# detect outliers in the test set
yhat = model.predict(testX)
# mark inliers 1, outliers -1
testy[testy == 1] = -1
testy[testy == 0] = 1
# calculate score
score = roc_auc_score(testy, yhat)
print('Auc Score: %.3f' % score)


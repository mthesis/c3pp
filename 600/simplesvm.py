import numpy as np

f=np.load("../599/evalb.npz")

cc=f["c"]
yy=f["y"]

cc=np.reshape(cc,[cc.shape[0],np.prod(cc.shape[1:])])

cc=cc[:,:4]

c=[]
y=[]

fractor=1
f2=1#00

for ac,ay in zip(cc,yy):
  if ay<0.5 or not np.random.randint(fractor):
    c.append(ac)
    y.append(ay)

c=np.array(c)
y=np.array(y)



# one-class svm for imbalanced binary classification
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.metrics import f1_score,auc,roc_auc_score
from sklearn.svm import OneClassSVM
# generate dataset
#X, y = make_classification(n_samples=10000, n_features=2, n_redundant=0,
#	n_clusters_per_class=1, weights=[0.999], flip_y=0, random_state=4)

X=c


# split into train/test sets
trainX, testX, trainy, testy = train_test_split(X, y, test_size=0.5, random_state=2, stratify=y)


trainX=np.array(list([ac for ac,ay in zip(trainX,trainy) if ay<0.5]))
trainy=np.array(list([ay for ay in trainy if ay<0.5]))




# define outlier detection model
model = OneClassSVM(gamma='scale', nu=0.5/(fractor*f2))
# fit on majority class
trainX = trainX[trainy==0]
model.fit(trainX)
# detect outliers in the test set
yhat = model.predict(testX)
# mark inliers 1, outliers -1
testy[testy == 1] = -1
testy[testy == 0] = 1
# calculate score
#score = f1_score(testy, yhat, pos_label=-1)
score = roc_auc_score(testy, yhat)
print('Auc Score: %.3f' % score)

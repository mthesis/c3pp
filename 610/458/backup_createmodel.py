import numpy as np
from numpy.random import randint as rndi
from numpy.random import random as rnd

from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer,Dense,Activation,Flatten,Dropout,Input,Concatenate,Dropout,Reshape
import tensorflow.keras as keras# as k
import tensorflow as t
from tensorflow.keras.models import Sequential,Model
from tensorflow.keras.optimizers import Adam,SGD,RMSprop
from tensorflow.keras.utils import plot_model

from gbuilder import *
from gtbuilder import *
from glbuilder import *
from gtlbuilder import *
from gcutter import *
from gpool import *
from gfeat import *
from gl import *
from gkeepbuilder import *
from glkeep import *
from gfeatkeep import *
from gkeepcutter import *
from gkeepmatcut import *
from gtopk import *
from gltk import *
from gltknd import *
from gpre1 import *
from gpre2 import *
from gpre3 import *
from gpre4 import *
from glmlp import *
from gltrivmlp import *
from gadd1 import *
from gaddzeros import *
from gsym import *
from gbrokengrowth import *
from gpoolgrowth import *
from gremoveparam import *
from gcomdepool import *
from gcompool import *
from gvaluation import *
from ggoparam import *
from gfromparam import *

objects={"gbuilder":gbuilder,"gtbuilder":gtbuilder,"glbuilder":glbuilder,"gtlbuilder":gtlbuilder,"gcutter":gcutter,"gpool":gpool,"gfeat":gfeat,"gl":gl,"gkeepbuilder":gkeepbuilder,"glkeep":glkeep,"gfeatkeep":gfeatkeep,"gkeepcutter":gkeepcutter,"gkeepmatcut":gkeepmatcut,"gtopk":gtopk,"gltk":gltk,"gltknd":gltknd,"gpre1":gpre1,"gpre2":gpre2,"gpre3":gpre3,"gpre4":gpre4,"glmlp":glmlp,"gltrivmlp":gltrivmlp,"gadd1":gadd1,"gaddzeros":gaddzeros,"gsym":gsym,"gbrokengrowth":gbrokengrowth,"gpoolgrowth":gpoolgrowth,"gremoveparam":gremoveparam,"gcompool":gcompool,"gcomdepool":gcomdepool,"gvaluation":gvaluation,"ggoparam":ggoparam,"gfromparam":gfromparam}



def schedule(epoch,lr):
  a=3E-4
  b=3E-5
  d=8
  d1=d
  d2=d
  c=5E-7
  tc=4
  if epoch==0:return a#kinds useless actually, but why not
  if epoch<d1:
    return a+(b-a)*(epoch/(d1-1))
  if epoch<d1+d2:
    return b+(a-b)*((epoch-d1)/(d2-1))
  if epoch>=d1+d2+tc:return c
  return a+(c-a)*((epoch-d1-d2)/(tc-1))
  

k=4
param0=3
param1=4
param2=5

dim1=16##gs
dim2=8

k1=k
k2=k

alin=[-1.0,1.0]
it=5#why does that kinda work, but 10 wont
it=1
flag=0

constparam=1
complexityparam=0


def createtestmodel(gs,out,n,**kwargs):
 
  dim1=gs
  dim2=gs

  ##encoder
  inputs=Input(shape=(gs,4,))
  feat0=gpre3(gs=gs)(inputs)

  mat1,feat=gtopk(gs=dim1,k=k1+1,param=param0,free=param1-param0,flag=flag,self_interaction=True)([feat0])
  feat2=glmlp(gs=dim1,param=param1,keepconst=constparam,iterations=it,alinearity=alin)([mat1,feat])
  feat3=gcutter(inn=dim1,out=dim2,param=param1)([feat2])

  mat2,feat4=gtopk(gs=dim2,k=k4+1,param=param1,free=param2-param1,flag=flag,self_interaction=True)([feat3])
  feat5a=gltk(gs=dim2,param=param2,keepconst=constparam,iterations=it,alinearity=alin)([mat2,feat4])
  feat5b=gltk(gs=dim2,param=param2,keepconst=constparam,iterations=it,alinearity=alin)([mat2,feat4])

  doa=Flatten()(feat5a)
  dob=Flatten()(feat5b)

  mats=[mat1,mat2]
  return Model(inputs=inputs,outputs=[feat0,mat1,feat,feat2,feat3,doa,dob])

def somegraphactions(q,n,gs,param,alin=[-1.0,1.0],keepconst=1,iterations=1,flag=0,k=8):#just a set of graph layers that should add complexity to the model without chaning dimensions in any way
  if n==0:return q
  mat1,feat1=gtopk(gs=gs,k=k,param=param,free=0,flag=flag,self_interaction=True)([q])
  feat2=gltknd(gs=gs,param=param,keepconst=constparam,iterations=it,alinearity=alin)([mat1,feat1])
  return somegraphactions(feat2,n=n-1,gs=gs,param=param,alin=alin,keepconst=keepconst,iterations=iterations,flag=flag,k=k)

def graphcompression(data,gs,param0,param1,c=2):
  feat0=gvaluation(gs=gs,param=param0)([data])
  feat1=gcompool(gs=gs,param=param0+1,paramo=param1,c=c)([feat0])
  return feat1
def graphdecompression(data,gs,param0,param1,c=2):
  feat0=gcomdepool(gs=gs,param=param0,paramo=param1,c=c)([data])
  return feat0


def compress(data,s,np,c):
  return graphcompression(data,s.gs,s.param,np,c),state(int(s.gs/c),np)
def decompress(data,s,np,c):
  return graphdecompression(data,s.gs,s.param,np,c),state(int(s.gs*c),np)
def actions(data,s,n=1,alin=[-1.0,1.0],keepconst=1,iterations=1,flag=0,k=8):
  return somegraphactions(data,n,s.gs,s.param,alin,keepconst,iterations,flag,k),s
def multidense(data,d,activation="relu",kernel_initializer=keras.initializers.TruncatedNormal(),):
  if len(d)==0 or True:return data
  ac=d.pop(0)
  data=Dense(ac,activation=activation,kernel_initializer=kernel_initializer)([data])
  return multidense(data,d,activation=activation,kernel_initializer=kernel_initializer)



class state:
  gs=10
  param=10
 

  def __init__(self,gs,param):
    self.gs=gs
    self.param=param

def createbothmodels(gs,out,n,**kwargs):
  ##trivial way
#  inputs,do1,do2=createmodel(gs,out,n,**kwargs)
#  output=createantimodel(gs,out,n,**kwargs)
#  return inputs,do1,do2,output
  
  assert gs==64




  alin=[-1.0,1.0]

  ns=       [1 ,1 ,1 ,1 ,1 ,1]
  its=      [1 ,1 ,1 ,1 ,1 ,1]
  ks=       [8 ,8 ,8 ,4 ,2 ,1]
  cs=       [2 ,2 ,2 ,2 ,2 ,2]
  #gs=       64 32 16 8  4  2              (before)
  params=[3 ,4 ,5 ,6 ,7 ,8 ,9]



  #needs to start with the input dimension, and end with the output dimension
  #(both get deleted in their specific cases)
  comp=[9,9,6,3]

  ins=ns.copy()
  ins.reverse()
  iits=its.copy()
  iits.reverse()
  iks=ks.copy()
  iks.reverse()
  ics=cs.copy()
  ics.reverse()
  iparams=params.copy()
  iparams.reverse()
  iparams.pop(0)

  decomp=comp.copy()
  decomp.reverse()
  comp.pop(0)
  decomp.pop(0)


  ##encoder
  inputs=Input(shape=(gs,4,))
  feat0=gpre4(gs=gs)(inputs)

  feat1=feat0

  s=state(gs=gs,param=params.pop(0))

  while len(params)>0:
    feat1,s=actions(feat1,s,n=ns.pop(0),alin=alin,iterations=its.pop(0),k=ks.pop(0))

    feat1,s=compress(feat1,s,params.pop(0),cs.pop(0))


  feat1=ggoparam(gs=1,param=s.param)([feat1])
  
  
  cdim=comp[-1]

  do0=multidense(feat1,comp.copy())
  doa=multidense(do0,[cdim])
  dob=multidense(do0,[cdim])

  




  mats=[]

  ##decoder
  inputs2=Input(shape=(cdim))
 
  taef0=multidense(inputs2,decomp)
  taef0=gfromparam(gs=s.gs,param=s.param)([taef0])

  taef1=taef0

  while len(iparams)>0:
    taef1,s=decompress(taef1,s,iparams.pop(0),ics.pop(0))
    taef1,s=actions(taef1,s,n=ins.pop(0),alin=alin,iterations=iits.pop(0),k=iks.pop(0))

  inn=[inputs2]

  #model=Model(inputs=inputs2,outputs=taef14,name="decoder")
  #returns input layer (this which we want to compare the the output), mean, logvar,decoder
  #return inputs,doa,dob,model
 

  #print("shall compare",feat0.shape,taef1.shape)
  #exit()

  return inputs,feat0,doa,dob,mats,inn,taef1
  #has to return:
    #input of encoder
    #comparison object
    #mean, logvar of encoder output
    #list of ajacency matrices
    #input of decoder
    #output of decoder




def createmodel(gs,out,n,**kwargs):#compression model,does not return any model, but two outputs, corresponding to mean and var, as well as the input variable used here

  inputs=Input(shape=(gs,4,))
  feat0=gpre2(gs=gs)(inputs)

  mat,feat=gtopk(gs=gs,k=k+1,param=param0,free=param1-param0,flag=paramm1-1,self_interaction=True)([feat0])
  feat2=gltk(gs=gs,param=param1,keepconst=paramm1,iterations=1,alinearity=[-1.0,1.0])([mat,feat])

  flatinn=gpool(gs=gs,param=param1)([feat2])
  
  d1=Dense(100,activation="relu")(flatinn)
  d2=Dense(10,activation="relu")(d1)
  do1=Dense(out,activation="relu")(d2)
  do2=Dense(out,activation="relu")(d2)
  return inputs,do1,do2

def createantimodel(gs,out,n,**kwargs):#decompression model

  inputs=Input(shape=(out,))

  d1=Dense(10,activation="relu")(inputs)
  d2=Dense(100,activation="relu")(d1)
  d3=Dense(gs*4,activation="relu")(d2)

  output=Reshape(target_shape=(gs,4))(d3)
  model=Model(inputs=inputs,outputs=output,name="decoder")
  return model














import numpy as np
import matplotlib.pyplot as plt

from cauc import caucd

from gram_schmidt import *

fn="evalb.npz"


shallplot=False


f=np.load(fn)


y=f["y"]
p=f["p"]






p=(p[np.where(y>0.5)]-1)


print(np.mean(np.corrcoef(np.transpose(p))))

q,r=gram_schmidt(np.copy(p))

q0=q

print(np.mean(np.corrcoef(np.transpose(q))))


print(f"p:{p.shape}")
print(f"q:{q.shape}")
print(f"r:{r.shape}")


q=np.dot(f["p"],np.linalg.inv(r))


qa=np.abs(q)

for idd in range(int(qa.shape[1])):
  acd=qa[:,idd]
  auc=caucd(d=acd,y=y)["auc"]
  print(f"i:{idd}=>auc:{auc:.6}    wid:{np.std(acd):.6} alt:{np.std(q0[:idd]):.6}")



if shallplot:
  plt.plot(np.arange(int(q.shape[0])),q,"o",alpha=0.5)

  plt.show()


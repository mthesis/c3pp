import numpy as np

from os import listdir
from os.path import isfile

from cauc import caucd

files=["aeval/"+q for q in listdir("aeval") if isfile("aeval/"+q)]

ps=[]

y=None


for fil in files[1:]:
  ac=np.load(fil)
  ps.append(ac["p"])
  if y is None:y=ac["y"]

ps=np.array(ps)

ps=np.prod(ps,axis=0)

d=np.abs(ps-1)


auc=caucd(d=d,y=y)["auc"]

print(auc)







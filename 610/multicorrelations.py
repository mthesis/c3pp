import numpy as np
import matplotlib.pyplot as plt

from cauc import caucd

import sys



i=[1,2]

if len(sys.argv)>1:
  i=[]
  for q in sys.argv[1:]:
    i.append(int(q))


fn=[f"multi/{ii}/evalb.npz" for ii in i]

f=[np.load(fnn) for fnn in fn]




y=f[0]["y"]


idd=2

p=[ff["p"][:,idd] for ff in f]

d=list([(ap-1)**2 for ap in p])

print("d correlations")

print(np.corrcoef(d))

aucs=[caucd(y=y,d=ad)["auc"] for ad in d]

print(" ".join([f"{auc:.4}" for auc in aucs]))


ds=np.sum(d,axis=0)

auc=caucd(y=y,d=ds)["auc"]

print(f"simple sum:{auc}")


w=[np.std(dd) for dd in d]


print(" ".join([f"{ww:.6}" for ww in w]))


exit()

id1=np.where(~np.isnan(y))

w1=np.std(d1[id1])
w2=np.std(d2[id1])
w=w1/w2

print(f"w1:{w1}  w2:{w2}  w1/w2:{w}")

exit()


powers=[-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3]

for power in powers:
  acd=d1+w**power*d2
  acauc=caucd(y=y,d=acd)["auc"]
  print(f"power={power}->auc={acauc}")

factors=np.arange(0.01,10,0.01)
aucs=np.zeros_like(factors)


for i,fact in enumerate(factors):
  aucs[i]=caucd(y=y,d=d1+fact*d2)["auc"]
  print(fact)


plt.plot(factors,aucs)

plt.show()



exit()

#plt.plot(np.arange(len(p1)),p1,"o",color="blue",alpha=0.5)
#plt.plot(np.arange(len(p2)),p2,"o",color="red",alpha=0.5)


#plt.show()










import numpy as np

def addevents(x,y,n=10000):
  m=np.mean(x,axis=0)
  s=np.std(x,axis=0)

  r=np.random.normal(m,s,(n,int(x.shape[1]),int(x.shape[2])))

  ry=np.zeros((n,))
  
  nx=np.concatenate((x,r),axis=0)
  ny=np.concatenate((y,ry),axis=0)

  seed=np.random.randint(10000)
  np.random.seed(seed)
  np.random.shuffle(nx)
  np.random.seed(seed)
  np.random.shuffle(ny)

  return nx,ny





if __name__=="__main__":
  addevents(np.random.normal(0,1,(5,6,7)),0)



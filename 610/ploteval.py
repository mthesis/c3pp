import numpy as np
import matplotlib.pyplot as plt
import sys


fn="evalb.npz"

f=np.load(fn)


y=f["y"]
p=f["p"]

p=np.mean(p,axis=-1)

ia=np.where(y<0.5)
ib=np.where(y>0.5)

pa=p[ia]
pb=p[ib]

rang=(np.min(p),np.max(p))


if len(sys.argv)>1:
  rang=(0.95,1.05)

plt.hist(pa,range=rang,bins=200,alpha=0.5,label="qcd")
plt.hist(pb,range=rang,bins=200,alpha=0.5,label="top")

plt.legend()

plt.show()





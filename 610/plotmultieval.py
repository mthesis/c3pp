import numpy as np
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile


for fn in ["aeval/"+q for q in listdir("aeval") if isfile("aeval/"+q)]:

#fn="evalb.npz"

  f=np.load(fn)


  y=f["y"]
  p=f["p"]

  p=np.mean(p,axis=-1)

  ia=np.where(y<0.5)
  ib=np.where(y>0.5)

  pa=p[ia]
  pb=p[ib]

  rang=(np.min(p),np.max(p))


  plt.hist(pa,range=rang,bins=200,alpha=0.5,label="qcd")
  plt.hist(pb,range=rang,bins=200,alpha=0.5,label="top")

  plt.legend()

  fo=fn.replace("aeval","imgs/peval/").replace(".npz",".png")
  plt.savefig(fo,format="png")
  plt.close()

  print("did",fn)





import numpy as np
from lossfunc import currentloss as cl



f=np.load("evalb.npz")

c=f["c"]
p=f["p"]



ltriv=cl(c,c,np)
lalt=cl(p,p,np)
la=cl(c,p,np)
lb=cl(p,c,np)


print(f"{ltriv} trivial loss ({lalt})")
print(f"{la} | {lb} in trained ")



ptri=cl(c,c,np,False)
pcla=cl(c,p,np,False)



i0=np.argmin(ptri)
i1=np.argmax(ptri)
l0=np.min(ptri)
l1=np.max(ptri)

print(f"minima:{l0} at {i0}")
print(f"maxima:{l1} at {i1}")


#import matplotlib.pyplot as plt
#plt.hist(ptri,bins=100)
#plt.show()

smal=pcla<ptri
numsmal=np.sum(smal)
frac=float(numsmal/len(pcla))

print(f"fraction of 'weird' losses {frac:.6}")

#print(len(np.where(~smal)))
#exit()

works=list(np.where(~smal)[0])[:20]

print("but those work",works)










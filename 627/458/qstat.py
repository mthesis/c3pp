import numpy as np
import csv as csv
from os.path import isfile


def loss(key="loss",fil="history"):
  with open(fil+".csv","r") as ff:
    c=csv.reader(ff,delimiter=",")
    jumped=False
    q=[]
    qn=[]
    epoch=[]
    for row in c:
      if not jumped:
        for e in row:
          q.append([])
          qn.append(e)
        jumped=True
        continue

      for i in range(len(row)):
        q[i].append(float(row[i]))
    for i in range(len(qn)):
      if qn[i]==key:
        return q[i]
 
def minloss():
  return np.min(loss("loss"))
def minvloss():
  return np.min(loss("val_loss"))
def roc(fil="roc"):
  if not isfile(fil+".npz"):return 0,0
  f=np.load(fil+".npz")
  return f["auc"],f["e30"]
def arb():return roc("data/arbite.npz")
def twoarb():return roc("data/twoarbite.npz")
def invarb():return roc("data/invarbite.npz")





def main():
  print("loss",minloss())
  print("val_loss",minvloss())
  auc,e30=roc()
  print("auc",auc)
  print("e30",1/(e30+0.0001))

if __name__ == "__main__":main()








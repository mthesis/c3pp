import numpy as np
import matplotlib.pyplot as plt


f=np.load("bcode.npz")


p=f["p"]
m1=f["m1"]
m2=f["m2"]
m3=f["m3"]
m4=f["m4"]
m5=f["m5"]
m6=f["m6"]




q=np.sum(p,axis=-1)
print(np.mean(q),np.std(q),np.min(q),np.max(q))


plt.hist(q,bins=20)

plt.savefig("imgs/sumhist.png",format="png")
plt.savefig("imgs/sumhist.pdf",format="pdf")

plt.show()


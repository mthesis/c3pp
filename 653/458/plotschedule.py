import numpy as np
import matplotlib.pyplot as plt

from createmodel import *


n=25

x=np.arange(n)


y=np.zeros(x.shape)


for i in range(n):
  y[i]=schedule(x[i],lr=1.0)


plt.plot(x,y)

plt.yscale("log",nonposy="clip")

plt.show()



















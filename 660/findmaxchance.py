import numpy as np
from os import listdir
import os.path as op





folder="aeval/"

files=list(listdir(folder))
files.sort()

max=[]
min=[]

x=[]
y=[]
ls=[]
cs=[]
ps=[]
indices=[]

for file in files:
  file=folder+file
  if not op.isfile(file):continue




  f=np.load(file)



  file=file[len(folder)+4:]
  file=file[:file.find(".")]

  indices.append(int(file))

  c=f["c"]
  p=f["p"]
  l=(c-p)**2

  l=np.mean(l,axis=(1,2))  

  ls.append(l)
  cs.append(c)
  ps.append(p)

  if len(x)==0:x=f["x"]
  if len(y)==0:y=f["y"]

  if len(max)==0:
    max=l
  else:
    max=np.max([max,l],axis=0)
  if len(min)==0:
    min=l
  else:
    min=np.min([min,l],axis=0)

  print(file)

ls=np.array(ls)
cs=np.array(cs)
ps=np.array(ps)

delta=max-min
delta=delta*(1-y)

i=np.argmax(delta)

print("ls",ls.shape)

lsi=ls[:,i]
print("lsi",lsi.shape)

csi=cs[:,i,:,:]
psi=ps[:,i,:,:]
print("csi",csi.shape)
print("psi",psi.shape)




np.savez_compressed("maxchance",i=i,l=lsi,c=csi,p=psi,x=x[i,:,:],y=y[i],ind=indices)

exit()


import matplotlib.pyplot as plt


print(i,delta[i],y[i])
plt.axvline(i)



plt.plot(delta)
plt.show()








































import numpy as np
import os
import sys

shallplot=False
if len(sys.argv)>1:
  shallplot=bool(sys.argv[1])


if os.path.isfile("bigevalb.npz"):
  f=np.load("bigevalb.npz")
else:
  print("loading evalb")
  f=np.load("evalb.npz")


c=f["c"]
p=f["p"]

n=c.shape[-1]

parts=[]
for i in range(n):
  ac=c[:,:,i]
  ap=p[:,:,i]
  ad=np.mean((ac-ap)**2)
  print("part",i,ad)
  parts.append(ad)


d=np.mean((c-p)**2)

dnull=np.mean((c-np.zeros_like(p))**2)

mean=np.mean(c,axis=0)
dmean=np.mean((mean-c)**2)

sc=c.copy()
np.random.shuffle(sc)

drandom=np.mean((sc-c)**2)
#drandom=np.mean((np.roll(c,axis=0,shift=np.random.randint(len(c)))-c)**2)



print("my loss           : ",d)
print("null hypothesis   : ",dnull)
print("mean hypothesis   : ",dmean)
print("random hypothesis : ",drandom)




if not shallplot:exit()




import matplotlib.pyplot as plt


fig=plt.figure()
ax1=fig.add_subplot(111)
ax2=ax1.twiny()

stdc="#0073e6"
otcol="#3399ff"
sort=[d,dnull,dmean,drandom]
bars=[d,dnull,dmean,drandom]
labs=["gae","null","mean","random"]
cols=[otcol,stdc,stdc,stdc]
#[stdc,"#1a8cff","#80bfff","#e6f2ff"]

subd=[0.3,0.35,0.4,0.45]

sort,bars,labs,cols=(list(t) for t in zip(*sorted(zip(sort,bars,labs,cols))))



for b,l,c,p in zip(bars,labs,cols,sort):
  #plt.bar(x=p,height=b,color=c,width=0.01,alpha=0.5)
  ax1.bar(x=l,height=b,color=c)

#plt.bar(x=[1],height=[2,1],alpha=1.0,color=["r","g"])

ax1.set_yscale("log")

if len(parts)==2:nams=["flag","pt"]
if len(parts)==4:nams=["flag","eta","phi","pt"]
if len(parts)==6:nams=["flag","eta","phi","m","E","pt"]


ax2.plot(nams[:len(parts)],parts,"o",markersize=12,color="red")


#ymin=np.min([np.max([np.mean(parts)-3*np.std(parts),np.min(parts)])],[np.min(bars)])
#print(ymin)
#ymax=np.max([np.min([np.mean(parts)+3*np.std(parts),np.max(parts)])],[np.max(bars)])
#print(ymax)

ymin=np.min([np.min(parts),np.min(bars)])
ymax=np.max([np.max(parts),np.max(bars)])
dy=ymax/ymin
ymin/=dy**0.1
ymax*=dy**0.1

ax1.set_ylim([ymin,ymax])


ax2.spines["bottom"].set_color(stdc)
ax2.spines["top"].set_color("red")

ax1.xaxis.label.set_color(stdc)
ax2.xaxis.label.set_color("red")


ax1.tick_params(axis="x",colors=stdc)
ax2.tick_params(axis="x",colors="red")

ax1.set_ylabel("loss")
ax1.set_xlabel("model")
ax2.set_xlabel("feature")

#plt.xscale("log",nonposx="clip")
#plt.xticks(sort,labs)

plt.savefig("imgs/reloss.png",format="png")
plt.savefig("imgs/reloss.pdf",format="pdf")

plt.show()



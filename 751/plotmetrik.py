import numpy as np
import matplotlib.pyplot as plt



metrik=np.load("metrikb.npz")["met"][0]


x=["E","p1","p2","p3"]
x=["flag","eta","phi","pt"]

plt.axhline(0,color="grey",alpha=0.2)

plt.ylabel("metrik")

plt.plot(x,metrik,"o")

plt.savefig("imgs/metrik.png",format="png")
plt.savefig("imgs/metrik.pdf",format="pdf")


plt.show()







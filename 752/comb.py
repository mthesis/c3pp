import numpy as np
import os
from os.path import isfile

from cauc import caucd

def statinf(q):
  return {"shape":q.shape,"mean":np.mean(q),"std":np.std(q),"min":np.min(q),"max":np.max(q)}


fs=list(["data/"+f for f in os.listdir("data") if isfile("data/"+f)])

fns=list([np.load(f) for f in fs])

aucs=list([float(f["auc"]) for f in fns])

print("aucs",aucs)
print(len(aucs))

pds=list([f["pd"] for f in fns])
wds=list([f["wd"] for f in fns])


sc=list([np.concatenate((pd,wd)) for pd,wd in zip(pds,wds)])

score=np.sum(sc,axis=0)
lab=np.concatenate((np.zeros_like(pds[0]),np.ones_like(wds[0])))

q=caucd(d=score,y=lab)

print(q["auc"])
print(q["e30"],q["i30"])

import sys
if len(sys.argv)<2:exit()

import matplotlib.pyplot as plt

range=(0,3)


plt.hist(score[np.where(lab<0.5)],bins=50,alpha=0.5,range=range,label="background")
plt.hist(score[np.where(lab>0.5)],bins=50,alpha=0.5,range=range,label="signal")

plt.legend()

plt.show()








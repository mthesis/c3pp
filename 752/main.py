import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.backend as K
from tensorflow.keras.layers import Layer, Input, Dense, Conv2D, MaxPooling2D, Flatten
from tensorflow.keras.models import Model
from tensorflow.keras.losses import mse
from tensorflow.keras.optimizers import Adam,SGD,RMSprop


import numpy as np
import matplotlib.pyplot as plt

import sys

idd=1
if len(sys.argv)>1:
  idd=int(sys.argv[1])


def statinf(q):
  return {"shape":q.shape,"mean":np.mean(q),"std":np.std(q),"min":np.min(q),"max":np.max(q)}


from tensorflow.keras.datasets import mnist

(x_train, y_train), (x_test, y_test) = mnist.load_data()
print(x_train.shape)
print(y_train.shape)
print(y_train[:100])



def normalise(q):
  ret=[]
  for aq in q:
    ret.append((aq-np.mean(aq))/255)
  qq=np.array(ret)
  return qq


def getdata(norm=True,normdex=7,n=1000):
  if norm:
    ids=np.where(y_train==normdex)
  else:
    ids=np.where(y_train!=normdex)
  qx=x_train[ids][:n]
  qy=np.reshape(qx,(int(qx.shape[0]),28*28))
  return normalise(qy),qx

normdex=7
t,rawt=getdata(norm=True,normdex=normdex,n=5000)
at,rawat=getdata(norm=False,normdex=normdex,n=5000)

print(statinf(t))
print(statinf(rawt))




class smult(Layer):
  def __init__(self,n=768,learnable=True,**kwargs):
    self.n=n
    super(smult,self).__init__(**kwargs)

  def build(self, input_shape):

    self.mult=self.add_weight(name="mult",
                                shape=(self.n,),
                                initializer="glorot_uniform",
                                trainable=True)



    self.built=True


  def call(self,x):
    x=x[0]
 
    #print("x",x.shape)
    #print("bias",self.bias.shape)
 
    ret=x*self.mult
    #print("ret",ret.shape) 

    return ret


    
  def compute_output_shape(self,input_shape):
    input_shape=input_shape[0]
    assert len(input_shape)==2
    assert input_shape[1]==self.n
    return tuple([input_shape[0],self.gs])    

  def get_config(self):
    mi={"n":self.n}
    th=super(smult,self).get_config()
    th.update(mi)
    return th
  def from_config(config):
    return smult(**config)







#def getmodel(qc,q,reg=None,act="relu",mean=1.0,seed=None):
def getmodel(q,reg=None,act="relu",mean=1.0,seed=None):
  np.random.seed(seed)
  #tf.random.set_seed(seed)
  tf.compat.v1.set_random_seed(seed)
  #inn=Input(shape=(28,28,1))
  inn=Input(shape=(784,))
  w=inn
  #w=inn
  #for a in qc:
  #  if a["typ"]=="conv":
  #    w=Conv2D(filters=a["newsize"],kernel_size=a["kernel"],activation=act)(w)
  #  if a["typ"]=="pool":
  #    w=MaxPooling2D((a["pool"],a["pool"]))(w)

  #w=Flatten()(w)
  for aq in q[1:]:
    w=Dense(aq,activation=act,use_bias=False,kernel_initializer=keras.initializers.TruncatedNormal(),kernel_regularizer=reg)(w)
  w=smult(n=q[-1])([w])
  m=Model(inn,w,name="oneoff")
  zero=K.ones_like(w)*mean
  loss=mse(w,zero)
  loss=K.mean(loss)
  m.add_loss(loss)
  m.compile(Adam(lr=0.01))
  return m



def regulariser(q):
  qt=K.transpose(q)
  return K.mean(K.abs(K.dot(q,qt)-1))


l=[784,1568,3920,3920,1568,784]
#m=getmodel(l,reg=regulariser,act="relu")
seed=idd
mo=getmodel(l,reg=None,act="relu",mean=1.0,seed=seed)
mp=np.mean(mo.predict(t))
m=getmodel(l,reg=None,act="relu",mean=1.0,seed=seed)
print(m)


cb=[keras.callbacks.EarlyStopping(monitor='val_loss',patience=20,restore_best_weights=True),
                   keras.callbacks.TerminateOnNaN()]
cb.append(keras.callbacks.ModelCheckpoint("models/model"+str(idd)+".tf", monitor='val_loss', verbose=1, save_best_only=True,save_weights_only=True))




m.summary()



mp=m.predict(t)
print(statinf(mp))
mop=mo.predict(t)
print(statinf(mop))


print("training on ",t.shape)
h=m.fit(t,None,
        epochs=500,
        batch_size=100,
        validation_split=0.25,
        verbose=1,
        callbacks=cb)


hist=h.history
plt.close()
for key in hist.keys():
  ac=hist[key][1:]
  plt.plot(np.arange(len(ac)),ac,label=key,alpha=0.8)
plt.legend()
plt.yscale("log",nonposy="clip")
plt.savefig(f"imgs/history{idd}.png",format="png")
plt.savefig(f"imgs/history{idd}.pdf",format="pdf")



model=h.model
p=model.predict(t)
print(statinf(p))
print(np.corrcoef(np.transpose(p)))


plt.close()
pp=np.mean(p,axis=-1)
print(statinf(pp))

plt.hist(pp,bins=100,alpha=0.5)
plt.savefig(f"imgs/pphist{idd}.png",format="png")
plt.savefig(f"imgs/pphist{idd}.pdf",format="pdf")



w=model.predict(at)
print(statinf(w))



plt.close()
ww=np.mean(w,axis=-1)
print(statinf(ww))

plt.hist(ww,bins=100,alpha=0.5)
plt.savefig(f"imgs/wwhist{idd}.png",format="png")
plt.savefig(f"imgs/wwhist{idd}.pdf",format="pdf")




plt.close()

plt.hist(pp,bins=100,alpha=0.5,label="background")
plt.hist(ww,bins=100,alpha=0.5,label="signal")
plt.legend()
plt.yscale("log",nonposy="clip")
plt.savefig(f"imgs/mmhist{idd}.png",format="png")
plt.savefig(f"imgs/mmhist{idd}.pdf",format="pdf")
plt.close()




from sklearn.metrics import roc_auc_score as auc


pd=np.abs(pp-1)
wd=np.abs(ww-1)
y_score=np.concatenate((pd,wd))
y_true=np.concatenate((np.zeros_like(pp),np.ones_like(ww)))
print(statinf(y_score))
print(statinf(y_true))


auc_score=auc(y_true,y_score)
print(f"reached auc of {auc_score}")



plt.close()
plt.hist(pd,bins=100,alpha=0.5,label="background")
plt.hist(wd,bins=100,alpha=0.5,label="signal")
plt.legend()
plt.savefig(f"imgs/aahist{idd}.png",format="png")
plt.savefig(f"imgs/aahist{idd}.pdf",format="pdf")
plt.close()


m=np.mean(pp)
pd=np.abs(pp-m)
wd=np.abs(ww-m)
y_score=np.concatenate((pd,wd))
y_true=np.concatenate((np.zeros_like(pp),np.ones_like(ww)))
print(statinf(pd))
print(statinf(wd))
print(statinf(y_score))
print(statinf(y_true))


plt.close()
plt.hist(pd,bins=100,alpha=0.5,label="background")
plt.hist(wd,bins=100,alpha=0.5,label="signal")
plt.legend()
plt.savefig(f"imgs/qqhist{idd}.png",format="png")
plt.savefig(f"imgs/qqhist{idd}.pdf",format="pdf")
plt.close()


auc_score=auc(y_true,y_score)
print(f"reached auc of {auc_score}")



print(f"saving output {idd}")
np.savez_compressed(f"data/s{idd}",pp=pp,ww=ww,m=m,pd=pd,wd=wd,auc=auc_score)
print(f"saved output {idd}")















import numpy as np
import matplotlib.pyplot as plt

plt.figure(figsize=(6,9))

def read(q):
  with open(q,"r") as f:
    return f.read()


cs=[]
aucs=[]
aucs2=[]

for i in range(280,288):
  c=read(str(i)+"/factor.py")
  c=float(c[7:])
  f=np.load(str(i)+"/data/qnll.npz")
  auc=f["raw"]
  auc2=f["ang"]
  cs.append(c)
  aucs.append(auc)
  aucs2.append(auc2)


plt.plot(cs,aucs,"o")

pfactor=0.2

factor=(1221.0/3109.0)**(1/3)

for f in range(1,9):
  plt.axvline(pfactor*(factor**f),color="grey",alpha=0.4)
plt.axhline(0.8961522198727851,color="grey",alpha=0.9,label="12 nodes splitted")

plt.xlabel("c")
plt.ylabel("auc")

plt.xscale("log",nonposx="clip")

plt.ylim([0.74,0.90])

plt.axhline(0.8558037029915226,color="grey",alpha=0.3,label="4 nodes")
plt.axhline(0.8772141072454867,color="grey",alpha=0.5,label="6 nodes")
plt.axhline(0.891101487923281,color="grey",alpha=0.7,label="8 nodes splittet")


plt.legend()

#plt.savefig("imgs/auwei.png",format="png")
#plt.savefig("imgs/auwei.pdf",format="pdf")

plt.plot(cs,aucs2,"o")

plt.legend()

#plt.savefig("imgs/auwei2.png",format="png")
#plt.savefig("imgs/auwei2.pdf",format="pdf")



plt.show()




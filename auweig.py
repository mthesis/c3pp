import numpy as np
import matplotlib.pyplot as plt

plt.figure(figsize=(6,9))

def read(q):
  with open(q,"r") as f:
    return f.read()


cs=[]
aucs=[]
aucs2=[]

for i in range(229,245):
  c=read(str(i)+"/factor.py")
  c=float(c[7:])
  auc=np.load(str(i)+"/roc.npz")["auc"]
  auc2=np.load(str(i)+"/rocp.npz")["auc"]
  cs.append(c)
  aucs.append(auc)
  aucs2.append(auc2)


plt.plot(cs,aucs,"o")

plt.axvline(3109.0/6745.0,color="grey",alpha=0.4)
plt.axhline(0.8911,color="grey",alpha=0.9,label="8 nodes splitted")

plt.xlabel("c")
plt.ylabel("auc")

plt.xscale("log",nonposx="clip")


plt.axhline(0.8558037029915226,color="grey",alpha=0.3,label="4 nodes")
plt.axhline(0.8772141072454867,color="grey",alpha=0.6,label="6 nodes")


plt.legend()

plt.savefig("imgs/auwei.png",format="png")
plt.savefig("imgs/auwei.pdf",format="pdf")

plt.plot(cs,aucs2,"o")

plt.legend()

plt.savefig("imgs/auwei2.png",format="png")
plt.savefig("imgs/auwei2.pdf",format="pdf")



plt.show()




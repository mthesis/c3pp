import numpy as np
import matplotlib.pyplot as plt

plt.figure(figsize=(6,9))

def read(q):
  with open(q,"r") as f:
    return f.read()


cs=[]
losss=[]
lossvs=[]

for i in range(229,245):
  c=read(str(i)+"/factor.py")
  c=float(c[7:])
  factor=1/(1+1/c)
  f=np.load(str(i)+"/data/min_loss.npz")
  loss=f["q"]*factor
  f=np.load(str(i)+"/data/min_val_loss.npz")
  lossv=f["q"]*factor
  cs.append(c)
  losss.append(loss)
  lossvs.append(lossv)


plt.plot(cs,losss,"o",label="loss")
plt.plot(cs,lossvs,"o",label="val_loss")

plt.axvline(3109.0/6745.0,color="grey",alpha=0.4)
#plt.axhline(0.8911,color="grey",alpha=0.9,label="8 nodes splitted")

plt.xlabel("c")
plt.ylabel("loss")

plt.xscale("log",nonposx="clip")

#plt.ylim([0.74,0.90])

#plt.axhline(0.8558037029915226,color="grey",alpha=0.3,label="4 nodes")
#plt.axhline(0.8772141072454867,color="grey",alpha=0.6,label="6 nodes")


plt.legend()

plt.savefig("imgs/auweiloss.png",format="png")
plt.savefig("imgs/auweiloss.pdf",format="pdf")



plt.show()




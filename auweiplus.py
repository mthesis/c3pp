import numpy as np
import matplotlib.pyplot as plt

plt.figure(figsize=(6,9))

def read(q):
  with open(q,"r") as f:
    return f.read()


cs=[]
aucs=[]
aucs2=[]

for i in range(247,263):
  c=read(str(i)+"/factor.py")
  c=float(c[7:])
  auc=np.load(str(i)+"/roc.npz")["auc"]
  auc2=np.load(str(i)+"/rocp.npz")["auc"]
  cs.append(c)
  aucs.append(auc)
  aucs2.append(auc2)


plt.plot(cs,aucs,"o")

plt.axvline(3109.0/6745.0,color="grey",alpha=0.4)
plt.axhline(0.8911,color="grey",alpha=0.4)

plt.xlabel("c")
plt.ylabel("auc")

plt.xscale("log",nonposx="clip")


#plt.savefig("imgs/auwei.png",format="png")
#plt.savefig("imgs/auwei.pdf",format="pdf")

plt.plot(cs,aucs2,"o")

#plt.savefig("imgs/auwei2.png",format="png")
#plt.savefig("imgs/auwei2.pdf",format="pdf")



plt.show()




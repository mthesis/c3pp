import numpy as np



fns=[581,582]

ps=np.array(list([np.load(f"{fn}/evalb.npz")["p"] for fn in fns]))

ps=np.mean(ps,axis=-1)

y=np.load(f"{fns[0]}/evalb.npz")["y"]

p0=np.array(list([p[np.where(y<0.5)] for p in ps]))
p1=np.array(list([p[np.where(y>0.5)] for p in ps]))


c=np.corrcoef(ps)
print("total")
print(c)

c0=np.corrcoef(p0)
print("qcd")
print(c0)

c1=np.corrcoef(p1)
print("top")
print(c1)




from test import *
import sys
import json


varfrac="varfrac"
id="id"
loss_l2="loss l2"
desc="desc"
failed="failed"
multi="multi"
auc="auc"
gs="gs"
varfracs="varfracs"
losses="losses"
mean="mean"
std="std"
min="min"
max="max"



sta=646
end=663

if len(sys.argv)>3:sta=int(sys.argv[3])
if len(sys.argv)>4:end=int(sys.argv[4])



q="d['failed']==False"
if len(sys.argv)>1:q=sys.argv[1]
what=""
if len(sys.argv)>2:what=sys.argv[2]


cou=0
cou2=0

for i in range(sta,end+1):
  d=analyse(i)
  if eval(q):
    cou+=1
    cou2+=d[multi]
    if len(what)==0:
      print(json.dumps(d,indent=2))
    elif "d[" in what:
      print(eval(what))
    elif what=="d":
      print(d)
    elif what=="ds":
      print({id:d[id],multi:d[multi],"vf":round(d[varfracs][max],2),desc:d[desc]})
    else:
      print(d[what])

print(f"in total {cou} ({cou2}) done")




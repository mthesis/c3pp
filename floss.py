import numpy as np
import matplotlib.pyplot as plt


def read(q):
  with open(q,"r") as f:
    return f.read()

def readspec(q):
  c=read(str(q)+"/spec.py")
  c=eval(c[5:])
  return c

def readaltspec(q):
  c=read("/home/sk656163/m/c1/"+str(q)+"/spec.py")
  c=eval(c[5:])
  return c


def read3(q):
  f1=np.load(str(q)+"/multi/1/loss.npz")["q"]
  f2=np.load(str(q)+"/multi/2/loss.npz")["q"]
  f3=np.load(str(q)+"/multi/3/loss.npz")["q"]


  
  xmin=np.min([f1,f2,f3])
  xmax=np.max([f1,f2,f3])
  return xmin,xmax

def read1(q):
  f=np.load(str(q)+"/data/min_val_loss.npz")["q"]
  return f

def readalt1(q):
  f=np.load("/home/sk656163/m/c1/"+str(q)+"/data/min_val_loss.npz")["q"]
  return f


minshallow,maxshallow,xshallow=[],[],[]

shallowdense=np.arange(323,336)

for d in shallowdense:
  xmin,xmax=read3(d)
  spec=np.prod(readspec(d))
  minshallow.append(xmin)
  maxshallow.append(xmax)
  xshallow.append(spec)

plt.plot(xshallow,minshallow,"o",color="green",label="min shallow")
plt.plot(xshallow,maxshallow,"o",color="green",label="max shallow")



yfixed,xfixed=[],[]

fixed=np.arange(292,303)

for f in fixed:
  x=np.prod(readspec(f))
  y=read1(f) 

  xfixed.append(x)
  yfixed.append(y)

plt.plot(xfixed,yfixed,"o",color="blue",label="linear graph")



xgraph,ygraph=[],[]

graph1=[203,206,207,208,209]
graph2=[198,199,200,201,202,204,205]


for g in graph2:
  x=np.prod(readaltspec(g))
  y=readalt1(g)
  xgraph.append(x)
  ygraph.append(y)
for g in graph1:
  x=np.prod(readspec(g))
  y=read1(g)
  xgraph.append(x)
  ygraph.append(y)


plt.plot(xgraph,ygraph,"o",color="red",label="const graph")


deepmin,deepmax,xdeep=[],[],[]

deep=np.arange(337,349)

for d in deep:
  dmin,dmax=read3(d)
  x=np.prod(readspec(d))
  xdeep.append(x)
  deepmin.append(dmin)
  deepmax.append(dmax)

plt.plot(xdeep,deepmin,"o",color="purple",label="min deep")
plt.plot(xdeep,deepmax,"o",color="purple",label="max deep")







plt.legend()

plt.show()






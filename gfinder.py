import numpy as np
import os
import re

from os import listdir
from os.path import isfile,join
from re import match

def isnumeric(q):
  """only checks for ints!!!"""
  try:
    p=int(q)
    return True
  except:
    return False

folders=[f for f in listdir(".") if (not isfile(f) and isnumeric(f))]

dic={}


for i,folder in enumerate(folders):
  files=[f for f in listdir(folder) if isfile(join(folder,f))]
  matches=[f for f in files if match("[g|d].*\.py",f)]
  for m in matches:
    if m in dic.keys():
      dic[m].append(int(folder))
    else:
      dic[m]=[int(folder)]
  print(f"did {i+1} of {len(folders)}")

for key in dic.keys():
  ac=dic[key]
  if 414 in ac:continue
  print(f"missing {key} out of {np.max(ac)}")








import numpy as np

from cauc import caucd
import sys

m=1
if len(sys.argv)>1:m=int(sys.argv[1])

if m==0:f1="404/multi/3"#top
if m==1:f1="405/multi/3"#qcd

if m==0:f2="408"#top
if m==1:f2="409/multi/2"#qcd


if m==0:l1=0.01166#top
if m==1:l1=0.01210#qcd

if m==0:l2=0.0178#top
if m==1:l2=0.03429#qcd



def getd(q):
  c=q["c"]
  p=q["p"]
  d=np.sqrt(np.mean((c-p)**2,axis=(1,2)))
  return d


fa=np.load(f1+"/evalb.npz")
fb=np.load(f2+"/evalb.npz")

y=fa["y"]

da=getd(fa)
db=getd(fb)


def aucbypower(p):
  d=da+db*(l2/l1)**p
  return caucd(d=d,y=y)["auc"]

#powers=np.arange(-5,5,0.1)
#aucs=np.zeros_like(powers)

#for i,p in enumerate(powers):
#  aucs[i]=aucbypower(p)
#  print("did",p)

#import matplotlib.pyplot as plt
#plt.plot(powers,aucs,"o")
#plt.show()





#exit()




d=da+db*(l2/l1)**(-3)

y=fa["y"]


print("A",caucd(d=da,y=y)["auc"])
print("B",caucd(d=db,y=y)["auc"])
print("+",caucd(d=d,y=y)["auc"])












import numpy as np
import matplotlib.pyplot as plt

from cauc import cauc,caucd

from graphs import getfolders

folders=getfolders()

def getspec(folder):
  with open(folder+"/spec.py","r") as f:
    return eval(f.read()[5:])
    #return spec
def getroc(folder):
  f=np.load(folder+"/roc.npz")
  return f
def geteval(folder):
  f=np.load(folder+"/evalb.npz")
  return f

def calcauc(f):
  c=f["c"][:,:,1:3]
  p=f["p"][:,:,1:3]
  y=f["y"]
  return cauc(c=c,p=p,y=y)



gss=[]
aucs=[]


for folder in folders:
  spec=getspec(folder)
  roc=getroc(folder)
  f=geteval(folder)
  roc=calcauc(f)
  auc=roc["auc"]
  gss.append(np.prod(spec))
  aucs.append(auc)
  print("did",folder)


plt.plot(gss,aucs,"o")

maxa=np.max(aucs)
maxg=gss[np.argmax(aucs)]

plt.axhline(maxa)
plt.axvline(maxg)

plt.title(str(round(maxa,6)))

plt.savefig("imgs/gang.png",format="png")
plt.savefig("imgs/gang.pdf",format="pdf")
np.savez_compressed("data/gang",gs=gss,auc=aucs)

plt.show()






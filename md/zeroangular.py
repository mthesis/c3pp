import numpy as np
import matplotlib.pyplot as plt

from cauc import cauc,caucd

from graphs import getfolders


fold="/home/sk656163/m/c1/195/"


def getspec(folder):
  with open(folder+"/spec.py","r") as f:
    return eval(f.read()[5:])
    #return spec
def getroc(folder):
  f=np.load(folder+"/roc.npz")
  return f
def geteval(folder):
  f=np.load(folder+"/evalb.npz")
  return f

def calcauc(f):
  c=f["c"]
  p=f["p"]
  y=f["y"]
  return cauc(c=c,p=p,y=y)

f=np.load(fold+"zout.npz")


gss=f["gss"]
aucs=f["aucs"]


plt.plot(gss,aucs,"o")

maxa=np.max(aucs)
maxg=gss[np.argmax(aucs)]

plt.axhline(maxa)
plt.axvline(maxg)

plt.title(str(round(maxa,6)))

plt.savefig("imgs/zang.png",format="png")
plt.savefig("imgs/zang.pdf",format="pdf")
np.savez_compressed("data/zang",gs=gss,auc=aucs)

plt.show()






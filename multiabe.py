import numpy as np
import matplotlib.pyplot as plt

import sys

mode=0
if len(sys.argv)>1:
  mode=int(sys.argv[1])

if mode==0:
  ids={581:"top",
       582:"qcd"}

if mode==1:
  ids={574:"top",
       575:"qcd"}

if mode==2:
  ids={575:"qcd"}

for idd in ids.keys():
  ac=np.load(f"{idd}/abe.npz")
  a=ac["a"]

  plt.plot(np.arange(len(a)),a,"o",label=ids[idd],alpha=0.5)


plt.axhline(0.5,color="grey",alpha=0.2)


if len(ids.keys())>1:plt.legend()

plt.ylabel("auc")
plt.xlabel("epoch")


plt.savefig(f"imgs/mabe{mode}.png",format="png")
plt.savefig(f"imgs/mabe{mode}.pdf",format="pdf")

plt.show()








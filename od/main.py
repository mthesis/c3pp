import numpy as np
import matplotlib.pyplot as plt

bp="../md/data/"
bp2="/home/sk656163/m/c1/tcroc/out/"
reff="/home/sk656163/m/rroc/QCDorWhat/"

def getref(q):
  r=np.load(reff+q+".npz")
  return r["auc"]




def save(q,loc=None):
  if loc!=None:
    plt.legend(loc=loc)
  else:
    plt.legend()
  plt.savefig("imgs/"+q+".png",format="png")
  plt.savefig("imgs/"+q+".pdf",format="pdf")

plt.figure(figsize=(8,6))



plt.title("Auc Scaling")
plt.axhline(getref("best_img"),color="grey",alpha=0.5,label="QCDorWhat")
plt.axhline(getref("worst_img"),color="grey",alpha=0.5)
plt.axhline(getref("Lola"),color="grey",alpha=0.5)


plt.axhline(0.5,color="grey",alpha=0.2)

plt.xlabel("gs")
plt.ylabel("auc")
plt.ylim([0.4,1.05])

save("emptyscale")


plt.plot([4,6],[0.8558,0.8772],"o",color="blue",label="my network")

save("lowscale")

plt.show()
exit()


plt.autoscale(True)

f=np.load(bp+"gtriv"+".npz")
plt.plot(f["gs"],f["auc"],label="Graph Network",color="Black",alpha=0.9,marker="o")



plt.xscale("log",nonposx="clip")

save("trivscale")




f=np.load(bp+"zang"+".npz")
plt.plot(f["gs"],f["auc"],label="compare angles to zero",color="DarkRed",alpha=0.9,marker=None)
save("compscale")


plt.xlim([2,16])
plt.ylim([0.6,0.9])
plt.xscale("linear")

save("compscale_zoom")

plt.autoscale(True)
plt.xscale("log",nonposx="clip")

f=np.load(bp2+"triv0.0"+".npz")
plt.plot(f["gs"],f["auc"],label="Splitted Graph Network",color="DarkBlue",alpha=0.9,marker="o")
save("splitscale")

f=np.load(bp2+"triv3.0"+".npz")
plt.plot(f["gs"],f["auc"],label="Scaled Graph Network",color="DarkGreen",alpha=0.9,marker="o")
save("superscale")

f=np.load(bp2+"ang3.0"+".npz")
plt.plot(f["gs"],f["auc"],label="Scaled Angular Graph Network",color="LimeGreen",alpha=0.9,marker="o")

plt.xlim([3,64])
plt.ylim([0.85,0.93])
save("angularscale")

plt.autoscale(True)

f=np.load(bp2+"pt3.0"+".npz")
plt.plot(f["gs"],f["auc"],label="Scaled pt Graph Network",color="blueviolet",alpha=0.9,marker="o")
save("ptscale")

f=np.load(bp+"dtriv"+".npz")
plt.plot(f["gs"],f["auc"],label="Simple Dense Network",color="orange",alpha=0.9,marker="o")
save("densescale",loc="lower right")




plt.show()






import numpy as np
import matplotlib.pyplot as plt
from os.path import isfile


x,y=[],[]
ya=[]
xo,yo=[],[]

for i,p in zip([0,1,15,16,17,4,5,6,7,8],[-9,-8,6,7,8,-5,-4,-3,-2,-1]):
  if isfile("359/multi/"+str(i)+"/qnll.npz"):
    f=np.load("359/multi/"+str(i)+"/qnll.npz")
    y.append(f["raw"])
    ya.append(f["ang"])
    x.append(p)
    
for i,p in zip([0,1,2,3,4,5,6,7,8,9,10],[-5,-4,-3,-2,-1,0,1,2,3,4,5]):
  if isfile("362/multi/"+str(i)+"/qnll.npz"):
    f=np.load("362/multi/"+str(i)+"/qnll.npz")
    y.append(f["raw"])
    ya.append(f["ang"])
    x.append(p)

for i,p in zip([0,1,15,16,17,4,5,6,7,8],[-9,-8,6,7,8,-5,-4,-3,-2,-1]):
  if isfile("359/multi/"+str(i)+"/roc.npz"):
    f=np.load("359/multi/"+str(i)+"/roc.npz")
    yo.append(f["auc"])
    xo.append(p)
    
for i,p in zip([0,1,2,3,4,5,6,7,8,9,10],[-5,-4,-3,-2,-1,0,1,2,3,4,5]):
  if isfile("362/multi/"+str(i)+"/roc.npz"):
    f=np.load("362/multi/"+str(i)+"/roc.npz")
    yo.append(f["auc"])
    xo.append(p)


plt.plot(x,y,"o",label="raw")
plt.plot(x,ya,"o",label="ang")
plt.plot(xo,yo,"o",label="unw")

plt.legend()

plt.show()





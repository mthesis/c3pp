import numpy as np
import matplotlib.pyplot as plt


def read(q):
  with open(q,"r") as f:
    return f.read()

def readspec(q):
  c=read(str(q)+"/spec.py")
  c=eval(c[5:])
  return c

def readaltspec(q):
  c=read("/home/sk656163/m/c1/"+str(q)+"/spec.py")
  c=eval(c[5:])
  return c


def read3(q):
  f1=np.load(str(q)+"/multi/1/loss.npz")["q"]
  f2=np.load(str(q)+"/multi/2/loss.npz")["q"]
  f3=np.load(str(q)+"/multi/3/loss.npz")["q"]


  
  xmin=np.min([f1,f2,f3])
  xmax=np.max([f1,f2,f3])
  return xmin,xmax
def readall3(q):
  f1=np.load(str(q)+"/multi/1/loss.npz")["q"]
  f2=np.load(str(q)+"/multi/2/loss.npz")["q"]
  f3=np.load(str(q)+"/multi/3/loss.npz")["q"]


  
  return f1,f2,f3


def read1(q):
  f=np.load(str(q)+"/data/min_val_loss.npz")["q"]
  return f

def readalt1(q):
  f=np.load("/home/sk656163/m/c1/"+str(q)+"/data/min_val_loss.npz")["q"]
  return f

def tripleread(q,xv,x,y):
  d1,d2,d3=readall3(q)

  x.append(xv)
  x.append(xv)
  x.append(xv)
  y.append(d1)
  y.append(d2)
  y.append(d3)

  return x,y

x=[]
y=[]

x,y=tripleread(325,"dense",x,y)
x,y=tripleread(352,"graph",x,y)
x,y=tripleread(353,"shuffle dense",x,y)
x,y=tripleread(350,"shuffle graph",x,y)
#x,y=tripleread(353,"353",x,y)
x,y=tripleread(356,"ortho dense",x,y)
x,y=tripleread(355,"ortho graph",x,y)
x,y=tripleread(358,"perm dense",x,y)
x,y=tripleread(357,"perm graph",x,y)

plt.plot(x,y,"o")



plt.show()



import numpy as np
import matplotlib.pyplot as plt
import csv


def read(q):
  with open(q,"r") as f:
    return f.read()

def readspec(q):
  c=read(str(q)+"/spec.py")
  c=eval(c[5:])
  return c

def readaltspec(q):
  c=read("/home/sk656163/m/c1/"+str(q)+"/spec.py")
  c=eval(c[5:])
  return c


def advreadloss(q):
  with open(str(q)+"/history.csv","r") as ff:
    c=csv.reader(ff,delimiter=",")
    jumped=False
    q=[]
    qn=[]
    epoch=[]
    for row in c:
      if not jumped:
        for e in row:
          q.append([])
          qn.append(e)
        jumped=True
        continue

      for i in range(len(row)):
        try:
          q[i].append(float(row[i]))
        except:
          q[i].append(1000000.0)

  for i in range(1,len(q)):
    if qn[i]=="val_loss":return np.min(q[i])
  return 0.0




def read3p(q):

  ret=[]
  ret.append(np.load(str(q)+"/roc.npz")["auc"])
  for i in range(1,1000):
    try:
      ret.append(np.load(str(q)+f'/multi/{i}/roc.npz')["auc"])
    except:
      break
  #print(len(ret))
  return ret



def readl3p(q):

  ret=[]
  ret.append(np.load(str(q)+"/data/loss.npz")["q"])
  for i in range(1,1000):
    try:
      ret.append(np.load(str(q)+f'/multi/{i}/loss.npz')["q"])
    except:
      break
  return ret
def readl3pp(q):

  ret=[]
  ret.append(advreadloss(q))
  for i in range(1,1000):
    try:
      ret.append(advreadloss(str(q)+f'/multi/{i}'))
    except:
      break
  return ret

def read1(q):
  f=np.load(str(q)+"/data/min_val_loss.npz")["q"]
  return f

def readalt1(q):
  f=np.load("/home/sk656163/m/c1/"+str(q)+"/data/min_val_loss.npz")["q"]
  return f



def plotone3p(q,nam):
  y=read3p(q)
  x=readl3pp(q)
  print(f"{nam}*{len(x)}::{np.argmin(x)}")
  #print(nam,x,y)
  plt.plot(x,y,"o",label=nam)


mode=0
import sys
if len(sys.argv)>1:
  mode=int(sys.argv[1]) % 3


dic={
404:"zeromean top",
405:"zeromean qcd",
406:"gs=6 top",
407:"gs=6 qcd",
408:"zeromean second batch top",
409:"zeromean second batch qcd",
410:"l1 top",
411:"l1 qcd"

}

for key in dic:
  plotone3p(key,dic[key])


plt.axhline(0.5,color="grey",alpha=0.5)

plt.legend()

plt.xlabel("loss")
plt.ylabel("auc")

plt.savefig("imgs/somecomp"+str(mode)+".png",format="png")
plt.savefig("imgs/somecomp"+str(mode)+".pdf",format="pdf")


plt.show()


import numpy as np
import matplotlib.pyplot as plt
import csv
import sys


def loadq(q):
  with open(str(q)+"/history_super.csv","r") as ff:
    c=csv.reader(ff,delimiter=",")
    jumped=False
    q=[]
    qn=[]
    epoch=[]
    for row in c:
      if not jumped:
        for e in row:
          q.append([])
          qn.append(e)
        jumped=True
        continue

      for i in range(len(row)):
        try:
          q[i].append(float(row[i]))
        except:
          q[i].append(-1)

  for i in range(1,len(q)):
    if qn[i]=="val_acc":return np.max(q[i])    
  return 0.0



q={
384:"ldm3",
386:"qcd3",
385:"ldm2",
388:"ldm1",
387:"qcd1",
389:"lmd0",
390:"qcd0",
391:"ldmpN0",
398:"qcdpN0",
393:"ldmN0",
394:"qcdN0",
399:"cdm0",


}


plt.figure(figsize=(10,6))


for key in q.keys():
  plt.plot([q[key]],[loadq(key)],"o",color="blue",alpha=0.5)


plt.axvline(1.5,color="grey",alpha=0.5)
plt.axvline(2.5,color="grey",alpha=0.5)
plt.axvline(4.5,color="grey",alpha=0.5)
plt.axvline(6.5,color="grey",alpha=0.5)
plt.axvline(8.5,color="grey",alpha=0.5)
plt.axvline(10.5,color="grey",alpha=0.5)

plt.axhline(0.5,color="grey",alpha=0.5)
#plt.axhline(0.6,color="grey",alpha=0.2)
#plt.axhline(0.7,color="grey",alpha=0.2)
#plt.axhline(0.8,color="grey",alpha=0.2)

plt.ylabel("Accuracy")


plt.savefig("imgs/superana.png",format="png")
plt.savefig("imgs/superana.pdf",format="pdf")


plt.show()












import numpy as np
import sys

import os
from os.path import isfile,isdir

import json

def read(fnam):
  with open(fnam,"r") as f:
    return f.read()


def loadq():
  arr=read("q").split("\n")
  arr=list([ac for ac in arr[2:] if " " in ac])
  d=dict({ac[:ac.find(" ")]:ac[ac.find(" ")+1:] for ac in arr})
  return d
theq={}
def assertheq():
  global theq
  if len(theq.keys())==0:theq=loadq()


def specvar(q):
  if len(q.shape)==2:
    q=np.reshape(q,(len(q),int(q.shape[1]/4),4))
  return np.mean(np.std(q,axis=1),axis=0) 

def calcvarfrac(f):
  wp=specvar(f["p"])
  wc=specvar(f["c"])
  return float(np.mean((wp/(wc+0.001))[1:]))

def calcloss(f):
  return float(np.mean((f["p"]-f["c"])**2))

def simplestat(q):
  return {"mean":float(np.mean(q)),"std":float(np.std(q)),"max":float(np.max(q)),"min":float(np.min(q)),"minid":float(np.argmin(q)),"maxid":float(np.argmax(q)),"data":q}

def emptystat():
  return {"mean":-1,"std":-1,"max":-1,"min":-1,"minid":-1,"maxid":-1,"data":[]}

def analyse(q):
  assertheq()
  ret={"failed":False,
       "id":q,
       "varfrac":-1,
       "desc":"none",
       "gs":-1,
       "loss l2":-1,
       "auc":100,
       "multi":-1,
       "varfracs":emptystat(),
       "losses":emptystat()}
  
  if not isdir(str(q)):
    ret["failed"]=True
    return ret
  if not isfile(str(q)+"/evalb.npz"):
    ret["failed"]=True
    return ret
  try:
    f=np.load(str(q)+"/evalb.npz")
    wp=specvar(f["p"])
    wc=specvar(f["c"])
    ret["id"]=int(q)
    ret["varfrac"]=calcvarfrac(f)
    ret["desc"]=theq[str(q)]
    ret["gs"]=float(np.prod(eval(read(str(q)+"/spec.py")[5:])))
    ret["loss l2"]=calcloss(f)
    varfracs=[ret["varfrac"]]
    losses=[ret["loss l2"]]
    if isfile(str(q)+"/roc.npz"):
      f2=np.load(str(q)+"/roc.npz")
      ret["auc"]=float(f2["auc"])
    
    if isdir(str(q)+"/multi"):
      ret["multi"]=len(os.listdir(str(q)+"/multi"))
      for d in os.listdir(str(q)+"/multi"):
        fnam=str(q)+"/multi/"+str(d)+"/evalb.npz"
        if not isfile(fnam):continue
        df=np.load(fnam)
        varfracs.append(calcvarfrac(df))
        losses.append(calcloss(df))
     
    ret["varfracs"]=simplestat(varfracs)
    ret["losses"]=simplestat(losses)
  
  except:
    print("ERROR",sys.exc_info())
    ret["failed"]=True
  return ret




if __name__=="__main__":
  q=624
  if len(sys.argv)>1:
    q=int(sys.argv[1])
  print(json.dumps(analyse(q),indent=2))

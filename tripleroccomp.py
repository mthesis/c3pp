import numpy as np
import matplotlib.pyplot as plt


def read(q):
  with open(q,"r") as f:
    return f.read()

def readspec(q):
  c=read(str(q)+"/spec.py")
  c=eval(c[5:])
  return c

def readaltspec(q):
  c=read("/home/sk656163/m/c1/"+str(q)+"/spec.py")
  c=eval(c[5:])
  return c


def read3p(q):
  f1=np.load(str(q)+"/multi/1/roc.npz")["auc"]
  f2=np.load(str(q)+"/multi/2/roc.npz")["auc"]
  f3=np.load(str(q)+"/multi/3/roc.npz")["auc"]
  f=np.load(str(q)+"/roc.npz")["auc"]

  return f1,f2,f3,f
  xmin=np.min([f1,f2,f3,f])
  xmax=np.max([f1,f2,f3,f])
  return xmin,xmax


def readl3p(q):
  f1=np.load(str(q)+"/multi/1/loss.npz")["q"]
  f2=np.load(str(q)+"/multi/2/loss.npz")["q"]
  f3=np.load(str(q)+"/multi/3/loss.npz")["q"]
  f=np.load(str(q)+"/data/loss.npz")["q"]

  return f1,f2,f3,f

def read1(q):
  f=np.load(str(q)+"/data/min_val_loss.npz")["q"]
  return f

def readalt1(q):
  f=np.load("/home/sk656163/m/c1/"+str(q)+"/data/min_val_loss.npz")["q"]
  return f



def plotone3p(q,nam):
  y1,y2,y3,y4=read3p(q)
  x1,x2,x3,x4=readl3p(q)
  x=[x1,x2,x3,x4]
  y=[y1,y2,y3,y4]
  plt.plot(x,y,"o",label=nam)


plotone3p(369,"top")
plotone3p(370,"qcd")
plotone3p(378,"lqcd")
plotone3p(379,"ldm")

plt.axhline(0.5,color="grey",alpha=0.5)

plt.legend()

plt.xlabel("loss")
plt.ylabel("auc")




plt.show()


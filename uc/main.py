import numpy as np
import matplotlib.pyplot as plt

from cauc import caucd


ff2="data/550.npz"         #pt like one, can be also more
ff2="data/evalb.npz"

ff1="data/ang3.0-10.npz"   #angular one

f1=np.load(ff1)
f2=np.load(ff2)


def getd(f):
  if "d" in f.keys():#ff1
    return np.ravel(f["d"]),[]
  else:#ff2
    p=f["p"]
    c=f["c"]
    #not so general, but cutting down to angles
    p=p[:,:,-1]
    c=c[:,:,-1]
    d=np.sqrt(np.mean((p-c)**2,axis=-1))
    return d,f["y"]
def norm(d):
  return (d-np.mean(d))/np.std(d)

d1,_=getd(f1)
d2,y=getd(f2)

print(d1.shape,np.mean(d1))
print(d2.shape,np.mean(d2))
print(y.shape)

d1=norm(d1)
d2=norm(d2)

corr=np.corrcoef(d1,d2)[0,1]
print("correlation",corr)

plt.hist(d1[np.where(y<0.5)],alpha=0.5,bins=50)
plt.hist(d1[np.where(y>0.5)],alpha=0.5,bins=50)
plt.show()

exit()


auc1=caucd(d=d1,y=y)["auc"]
auc2=caucd(d=d2,y=y)["auc"]

print(auc1)
print(auc2)

cs=np.arange(-0.01,0.01,0.0005)
aucs=np.zeros_like(cs)

for i,c in enumerate(cs):

  d=d1+c*d2
  auc=caucd(d=d,y=y)["auc"]
  aucs[i]=auc

  print(c,auc)


plt.plot(cs,aucs)
plt.show()


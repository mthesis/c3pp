import numpy as np
import matplotlib.pyplot as plt

from cauc import caucd


ff2="data/550.npz"         #pt like one, can be also more
ff2="data/evalb.npz"

ff1="data/ang3.0-10.npz"   #angular one

f1=np.load(ff1)
f2=np.load(ff2)


def getd(f):
  if "d" in f.keys():#ff1
    return np.ravel(f["d"]),[]
  else:#ff2
    p=f["p"]
    c=f["c"]
    #not so general, but cutting down to angles
    p=p[:,:,-1]
    c=c[:,:,-1]
    d=np.sqrt(np.mean((p-c)**2,axis=-1))
    return d,f["y"]
def norm(d):
  return (d-np.mean(d))/np.std(d)

d1,_=getd(f1)
d2,y=getd(f2)

print(d1.shape,np.mean(d1))
print(d2.shape,np.mean(d2))
print(y.shape)

d1=norm(d1)
d2=norm(d2)

corr=np.corrcoef(d1,d2)[0,1]
print("correlation",corr)

def dp(d1,p):
  ret=np.zeros_like(d1)
  for i in range(len(d1)):
    try:
      ret[i]=np.abs(d1[i])**p
    except:
      print("failed",d1[i],p)
  return ret


print(caucd(d=dp(d1,1),y=y)["e30"])
print(caucd(d=dp(d1,1.5),y=y)["e30"])
print(caucd(d=dp(d1,2.0),y=y)["e30"])
print(caucd(d=np.sin(d1),y=y)["e30"])
print(caucd(d=d1,y=y)["e30"])

omegas=np.arange(1.0,2.0,0.025)
e30s=[]

for omega in omegas:
  ace30=caucd(d=np.sin(d1*omega),y=y)["e30"]
  e30s.append(ace30)
  print(omega,ace30)

plt.plot(omegas,e30s)
plt.show()


exit()

powers=np.arange(0.1,2.0,0.1)
e30s=np.zeros_like(powers)


for i,power in enumerate(powers):
  ace30=caucd(d=np.power(d1,power),y=y)["e30"]
  e30s[i]=ace30
  print(power,ace30)


plt.plot(powers,e30s)
plt.show()





